# Aliases!

alias ..="cd .."
alias ...="cd ..; cd .."
alias ....="cd ..; cd ..; cd .."
alias .....="cd ..; cd ..; cd ..; cd .."


alias cdb="cd ~/sga/repos/build"

alias ll="ls -alF"

#Git
alias gits="git status"
alias gitka="gitk --all &"
alias gitaa="git add --all && git commit --amend"

#Code
alias c="code ."

# pacman, https://wiki.archlinux.org/title/Pacman#Upgrading_packages

alias upgrade="sudo pacman -Syu"

# Startup

python ~/simons-personal-repo/newterminal.py
