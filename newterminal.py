# Script to run when opening a new terminal!

import shutil
import random
import sys
from datetime import datetime
from termcolor import colored


# Print different greetings depending on time of the day
now = datetime.now()
current_hour = int(now.strftime("%H"))
greeting = "+++ Divide By Cucumber Error. Please Reinstall Universe And Reboot +++"

if (current_hour > 0 and current_hour <= 6):
    greeting = "You should be sleeping, sir!"
elif (current_hour > 6 and current_hour <= 11):
    greeting = "Good morning, sir."
elif (current_hour > 11 and current_hour <= 13):
    greeting = "You should be having lunch, sir!"
elif (current_hour > 13 and current_hour <=17):
    greeting = "Good afternoon, sir."
elif (current_hour > 17 and current_hour <=23):
    greeting = "Good evening, sir."


columns = shutil.get_terminal_size().columns
print(colored(greeting, 'green', attrs=['bold']).center(columns))

#Lets print a Pratchett quote while we are at it.
#File found here: https://www.lspace.org/books/pqf/

quotes_file = open(sys.path[0] + "/pqf", "r")
quotes = quotes_file.read()
quotes_list = quotes.split("\n\n")

print(quotes_list[random.randint(0, len(quotes_list))], "\n")



